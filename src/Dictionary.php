<?php

namespace Vulpes\Language;

use ArrayAccess;

class Dictionary implements ArrayAccess
{
    protected string $domain;
    protected string $language;
    protected array $storage;

    public function __construct(string $domain, string $language, array $storage)
    {
        $this->domain = $domain;
        $this->language = $language;
        $this->storage = $storage;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function toArray(): array
    {
        return $this->storage;
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->storage);
    }

    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->storage[$offset] : $offset;
    }

    public function offsetSet($offset, $value) { }

    public function offsetUnset($offset) { }
}