<?php

namespace Vulpes\Language;

/**
 * Interface to provide accessing to language data
 * @see \Vulpes\Language\Language
 */
interface LanguageAccess
{
    public function getDomain(): string;

    public function getDomains(): array;

    public function setDomain(string $domain);

    public function getLanguage(): string;

    public function getRegion(): string;

    public function isLanguage(string $language): bool;

    public function getLanguages(): array;

    public function setLanguage(string $language);

    public function getLanguageValues(string $domain, string $language): array;
}