<?php

namespace Vulpes\Language;

class Language extends Dictionary implements LanguageAccess
{
    protected LanguageAccess $access;

    public function __construct(LanguageAccess $access)
    {
        $this->access = $access;
        parent::__construct($this->getDomain(), $this->getLanguage(),
          $this->access->getLanguageValues($this->getDomain(), $this->getLanguage()));
    }

    public function getDomain(): string
    {
        return $this->access->getDomain();
    }

    public function getDomains(): array
    {
        return $this->access->getDomains();
    }

    public function setDomain(string $domain): void
    {
        $this->access->setDomain($domain);
        $this->init();
    }

    public function getLanguage(): string
    {
        return $this->access->getLanguage();
    }

    public function getRegion(): string
    {
        return $this->access->getRegion();
    }

    public function getLanguages(): array
    {
        return $this->access->getLanguages();
    }

    public function isLanguage(string $language): bool
    {
        return $this->access->isLanguage($language);
    }

    public function setLanguage(string $language): void
    {
        $this->access->setLanguage($language);
        $this->init();
    }

    private function init(): void
    {
        if ($this->domain === $this->getDomain() && $this->language === $this->getLanguage()) {
            return;
        }

        $this->domain = $this->getDomain();
        $this->language = $this->getLanguage();

        $this->storage = $this->access->getLanguageValues($this->domain, $this->language);
    }

    public function getLanguageValues(string $domain, string $language): array
    {
       return $this->access->getLanguageValues($domain, $language);
    }

    public function createDictionary(?string $domain, ?string $language): Dictionary
    {
        is_null($domain) && $domain = $this->getDomain();
        is_null($language) && $language = $this->getLanguage();

        return new Dictionary($domain, $language, $this->getLanguageValues($domain, $language));
    }
}